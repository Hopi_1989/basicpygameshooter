import os
import random
import pygame

import PlayerInput
import PlayerManager
import EnemyManager
import BulletManager
import PowerupManager
import UserInterface
import EffectManager
from Constants import SCREEN_HEIGHT, SCREEN_WIDTH, FRAME_RATE


def run_game() -> None:
    random.seed()
    pygame.init()
    clock = pygame.time.Clock()
    pygame.mouse.set_visible(False)

    app_icon = pygame.image.load(os.path.join("Assets", "Images", "sprite_player.png"))
    pygame.display.set_icon(app_icon)
    pygame.display.set_caption("Space Shooter")

    player_input = PlayerInput.PlayerInput.get_instance()
    user_interface = UserInterface.UserInterface.get_instance()
    player_manager = PlayerManager.PlayerManager.get_instance()
    enemy_manager = EnemyManager.EnemyManager.get_instance()
    bullet_manager = BulletManager.BulletManager.get_instance()
    powerup_manager = PowerupManager.PowerupManager.get_instance()
    effect_manager = EffectManager.EffectManager.get_instance()

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    background = pygame.transform.scale(
        pygame.image.load(os.path.join("Assets", "Images", "bg_background.jpeg")),
        (SCREEN_WIDTH, SCREEN_HEIGHT))

    current_round = 1
    enemy_manager.set_round(current_round)
    user_interface.set_round(current_round)
    user_interface.set_game_state(UserInterface.GameState.INTRO)

    is_running = True
    while is_running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False

        player_input.update()
        user_interface.update()
        effect_manager.update()

        if user_interface.get_game_state() == UserInterface.GameState.INTRO:
            pass  # don't do anything during intro state

        if user_interface.get_game_state() == UserInterface.GameState.UNPAUSED:
            player_manager.update()
            enemy_manager.update()
            bullet_manager.update()
            powerup_manager.update()

            if player_manager.get_player().is_alive() and enemy_manager.all_enemies_destroyed():
                current_round += 1
                enemy_manager.set_round(current_round)
                user_interface.set_round(current_round)
                user_interface.set_game_state(UserInterface.GameState.ROUND_CLEAR)

        if user_interface.get_game_state() == UserInterface.GameState.PAUSED:
            pass  # don't do anything if paused

        if user_interface.get_game_state() == UserInterface.GameState.GAME_OVER:
            pass  # don't do anything if gameover

        if user_interface.get_game_state() == UserInterface.GameState.RESET:
            current_round = 1
            player_manager.respawn()
            powerup_manager.reset()
            bullet_manager.reset()

            enemy_manager.reset()
            enemy_manager.set_round(current_round)
            user_interface.reset()
            user_interface.set_round(current_round)

        if user_interface.get_game_state() == UserInterface.GameState.ROUND_CLEAR:
            player_manager.update()
            bullet_manager.update()
            powerup_manager.update()

        if user_interface.get_game_state() == UserInterface.GameState.EXIT:
            is_running = False

        # Begin Draws
        screen.blit(background, (0, 0))

        effect_manager.render()
        powerup_manager.render()
        bullet_manager.render()
        enemy_manager.render()
        player_manager.render()
        user_interface.render()

        pygame.display.flip()
        clock.tick(FRAME_RATE)

    shutdown()


def shutdown() -> None:
    pygame.quit()
