import math
import pygame


class Point:
    def __init__(self, x: float = 0, y: float = 0):
        self.x = x
        self.y = y

    def clone(self) -> 'Point':
        return Point(self.x, self.y)

    def distance_to(self, other: pygame.Vector2) -> float:
        x = abs(other.x - self.x) ** 2.0
        y = abs(other.y - self.y) ** 2.0
        return math.sqrt(x + y)

    def as_vector2(self) -> pygame.Vector2:
        return pygame.Vector2(self.x, self.y)

    def as_tuple(self) -> tuple[float, float]:
        return self.x, self.y

    def __add__(self, other: 'Point') -> 'Point':
        return Point(self.x + other.x, self.y + other.y)
