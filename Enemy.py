import os
import enum
import pygame
import Entity
import MyUtils
import Scoreboard
import PowerupManager
import PlayerManager
import BulletManager
import EffectManager


class EnemyType(enum.Enum):
    AGGRO = 1
    PROJECTILE = 2
    BOSS = 3


class Enemy(Entity.Entity):
    SPRITE_AGGRO: pygame.Surface = None
    SPRITE_PROJECTILE: pygame.Surface = None
    SPRITE_BOSS: pygame.Surface = None
    SPRITE_EXHAUST: list[pygame.Surface] = []

    RADIUS_AGGRO: int = 15
    RADIUS_PROJECTILE: int = 25
    RADIUS_BOSS: int = 100

    # used for PROJECTILE type enemy
    MIN_DISTANCE: int = 300
    MAX_DISTANCE: int = 350
    SHOOT_COOLDOWN: int = 45
    DAMAGE_REDUCTION_PROJECTILE: int = 5

    # used for BOSS type enemy
    DAMAGE_REDUCTION_BOSS: int = 23
    HEALTH_GATE_ONE: int = 70
    HEALTH_GATE_TWO: int = 30

    def __init__(self, position: MyUtils.Point, move_speed: int = 1, alive: bool = False):
        Entity.Entity.__init__(self, position, Enemy.RADIUS_AGGRO, move_speed, alive)
        self._destroyed = False
        self._enemy_type = EnemyType.AGGRO
        self._exaust_frame = 0
        self._shoot_cooldown = 0
        self._shielded = False
        self._next_health_gate = Enemy.HEALTH_GATE_ONE
        self._bullet_manager = BulletManager.BulletManager.get_instance()
        self._effect_manager = EffectManager.EffectManager.get_instance()

    def set_shield(self, active: bool) -> None:
        self._shielded = active

    def die(self) -> None:
        if self._enemy_type == EnemyType.BOSS:
            point_value = self._move_speed * 250
            self._effect_manager.create_enemy_explosion(self._position.clone(), 6.0)
            PowerupManager.PowerupManager.get_instance().spawn_health_pack(self._position.clone())
        else:
            point_value = self._move_speed * 50
            explosion_scale = 1.5 if self._enemy_type == EnemyType.PROJECTILE else 1
            self._effect_manager.create_enemy_explosion(self._position.clone(), explosion_scale)
            PowerupManager.PowerupManager.get_instance().enemy_destroyed(self._position.clone())

        self._destroyed = True
        Scoreboard.Scoreboard.get_instance().increment_score(point_value)
        Entity.Entity.die(self)

    def take_damage(self, amount: int) -> None:
        if self._enemy_type == EnemyType.PROJECTILE:
            amount -= Enemy.DAMAGE_REDUCTION_PROJECTILE
        if self._enemy_type == EnemyType.BOSS:
            if self._shielded:
                return
            amount -= Enemy.DAMAGE_REDUCTION_BOSS

        Entity.Entity.take_damage(self, amount)

    def collided_with_player(self) -> None:
        self._destroyed = True
        self._effect_manager.create_enemy_explosion(self._position.clone())
        Entity.Entity.die(self)

    def was_destroyed(self) -> bool:
        if self._destroyed:
            self._destroyed = False
            return True
        return False

    def respawn_type(self, position: MyUtils.Point, move_speed: int = 1, enemy_type: EnemyType = EnemyType.AGGRO) -> None:
        Entity.Entity.respawn(self, position, move_speed)
        self._enemy_type = enemy_type
        self._radius = self.get_radius()

        if self._enemy_type == EnemyType.BOSS:
            self._shielded = False
            self._next_health_gate = Enemy.HEALTH_GATE_ONE

    def get_radius(self) -> int:
        """
        returns the radius to be used for collision with bullets, or player

        :return: integer representing collision radius
        """
        if self._enemy_type == EnemyType.PROJECTILE:
            return Enemy.RADIUS_PROJECTILE - 5
        if self._enemy_type == EnemyType.BOSS:
            return Enemy.RADIUS_BOSS - 50
        return Enemy.RADIUS_AGGRO

    def reset(self) -> None:
        self._alive = False
        self._position.x = 0
        self._position.y = 0
        self._move_speed = 0
        self._destroyed = False

    def update(self) -> None:
        if not self._alive:
            return

        player = PlayerManager.PlayerManager.get_instance().get_player()
        if player.is_alive():
            self.look_at(player.get_position().as_vector2())

        if self._enemy_type == EnemyType.AGGRO:
            self._seek_target(player)
            if self._collided_with_target(player):
                player.take_damage(10)
                self.collided_with_player()

        if self._enemy_type == EnemyType.PROJECTILE:
            self.get_into_range(player)
            if self.can_shoot():
                self._shoot_cooldown = Enemy.SHOOT_COOLDOWN
                self._bullet_manager.fire_enemy_bullet(self._position.clone(), self._direction_to_target(player))

        if self._enemy_type == EnemyType.BOSS:
            self.get_into_range(player)
            if self.can_shoot():
                self._shoot_cooldown = Enemy.SHOOT_COOLDOWN
                self._bullet_manager.fire_enemy_bullet(self._position.clone(), self._direction_to_target(player), True)

        self.constrain_to_view(MyUtils.Point(self._radius * 4, self._radius * 4))

    def get_into_range(self, target: Entity.Entity) -> None:
        distance_to_player = self._position.distance_to(target._position.as_vector2())
        if distance_to_player > Enemy.MAX_DISTANCE:
            self._seek_target(target)
        if distance_to_player < Enemy.MIN_DISTANCE:
            self._avoid_target(target)

    def can_shoot(self) -> bool:
        self._shoot_cooldown -= 1
        if self._shoot_cooldown < 0:
            return True
        return False

    def spawn_minions(self) -> bool:
        if not self._alive:
            return False

        if self._health <= self._next_health_gate:
            if self._next_health_gate == Enemy.HEALTH_GATE_ONE:
                self._next_health_gate = Enemy.HEALTH_GATE_TWO
            else:
                self._next_health_gate = 0
            return True
        return False

    def _collided_with_target(self, target: Entity.Entity) -> bool:
        radius = self._radius + (target.get_radius() * 0.5)  # give some forgiveness on collision
        if self._position.distance_to(target.get_position().as_vector2()) < radius:
            return True
        return False

    def _seek_target(self, target: Entity.Entity) -> None:
        direction_to_target = self._direction_to_target(target)
        self._position.x += (direction_to_target.x * self._move_speed)
        self._position.y += (direction_to_target.y * self._move_speed)

    def _avoid_target(self, target: Entity.Entity) -> None:
        direction_to_target = self._direction_to_target(target)
        self._position.x -= (direction_to_target.x * self._move_speed)
        self._position.y -= (direction_to_target.y * self._move_speed)

    def _direction_to_target(self, target: Entity.Entity) -> pygame.Vector2:
        x = target.get_position().x - self._position.x
        y = target.get_position().y - self._position.y
        direction = pygame.Vector2(x, y)
        return direction.normalize()

    def render(self) -> None:
        if self._alive:
            self.render_exaust()

            render_sprite = pygame.transform.rotate(self.get_sprite(), self._rotation)
            rect = render_sprite.get_rect()
            rect.x = self._position.x - (render_sprite.get_rect().width / 2)
            rect.y = self._position.y - (render_sprite.get_rect().height / 2)
            pygame.display.get_surface().blit(render_sprite, rect)

            if self._enemy_type == EnemyType.BOSS:
                if self._shielded:
                    pygame.draw.circle(pygame.display.get_surface(), (255, 255, 255), (self._position.x, self._position.y), self._radius * 2, 5)
                return

            # render non-boss healthbar last
            Entity.Entity.render(self)

    def render_exaust(self) -> None:
        self._exaust_frame += 1
        if self._exaust_frame >= len(Enemy.SPRITE_EXHAUST):
            self._exaust_frame = 0

        if self._enemy_type == EnemyType.BOSS:
            exhaust_offset = self.get_exaust_offset()
            render_sprite = pygame.transform.rotate(Enemy.SPRITE_EXHAUST[self._exaust_frame], self._rotation)
            rect = render_sprite.get_rect()

            exhaust_offset.x = 45
            right_offset = exhaust_offset.rotate(-self._rotation)
            rect.x = self._position.x - (rect.width / 2) + right_offset.x
            rect.y = self._position.y - (rect.height / 2) + right_offset.y
            pygame.display.get_surface().blit(render_sprite, rect)

            exhaust_offset.x = -45
            left_offset = exhaust_offset.rotate(-self._rotation)
            rect.x = self._position.x - (rect.width / 2) + left_offset.x
            rect.y = self._position.y - (rect.height / 2) + left_offset.y
            pygame.display.get_surface().blit(render_sprite, rect)
            return

        exhaust_offset = self.get_exaust_offset().rotate(-self._rotation)
        render_sprite = pygame.transform.scale(Enemy.SPRITE_EXHAUST[self._exaust_frame], (25, 25))
        render_sprite = pygame.transform.rotate(render_sprite, self._rotation)
        rect = render_sprite.get_rect()
        rect.x = self._position.x - (rect.width / 2) + exhaust_offset.x
        rect.y = self._position.y - (rect.height / 2) + exhaust_offset.y
        pygame.display.get_surface().blit(render_sprite, rect)

    def get_sprite(self) -> pygame.Surface | None:
        if self._enemy_type == EnemyType.AGGRO:
            return Enemy.SPRITE_AGGRO
        if self._enemy_type == EnemyType.PROJECTILE:
            return Enemy.SPRITE_PROJECTILE
        if self._enemy_type == EnemyType.BOSS:
            return Enemy.SPRITE_BOSS

        return None

    def get_exaust_offset(self) -> pygame.Vector2:
        if self._enemy_type == EnemyType.AGGRO:
            return pygame.Vector2(0, 22)
        if self._enemy_type == EnemyType.PROJECTILE:
            return pygame.Vector2(0, 28)
        if self._enemy_type == EnemyType.BOSS:
            return pygame.Vector2(0, 90)

        return pygame.Vector2(0, 0)

    @staticmethod
    def load_sprites() -> None:
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "sprite_enemy_aggro.png"))
        Enemy.SPRITE_AGGRO = pygame.transform.scale(sprite_temp, (Enemy.RADIUS_AGGRO * 2, Enemy.RADIUS_AGGRO * 2))

        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "sprite_enemy_projectile.png"))
        Enemy.SPRITE_PROJECTILE = pygame.transform.scale(sprite_temp, (Enemy.RADIUS_PROJECTILE * 2, Enemy.RADIUS_PROJECTILE * 2))

        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "sprite_boss.png"))
        Enemy.SPRITE_BOSS = pygame.transform.scale(sprite_temp, (Enemy.RADIUS_BOSS * 2, Enemy.RADIUS_BOSS * 2))

        # exhaust animation
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "Animations", "sprite_flame_01.png"))
        Enemy.SPRITE_EXHAUST.append(pygame.transform.scale(sprite_temp, (50, 50)))
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "Animations", "sprite_flame_02.png"))
        Enemy.SPRITE_EXHAUST.append(pygame.transform.scale(sprite_temp, (50, 50)))
