import os
import pygame
import Entity
import MyUtils
import Powerup
import PlayerInput
import UserInterface
import SoundManager
import BulletManager
import PowerupManager
import EffectManager


class Player(Entity.Entity):
    SHIELD_RADIUS = 25
    SHIELD_RADIUS_THICKNESS = 20
    PLAYER_RADIUS: int = 25
    PLAYER_MOVE_SPEED: int = 10
    PLAYER_SHOOT_COOLDOWN = 12
    PLAYER_SHOOT_COOLDOWN_POWERUP = 4
    PLAYER_SHOOT_SCATTER_CONE = 10
    SHIELD_COLOR = (0, 0, 255, 50)

    def __init__(self, position: MyUtils.Point):
        Entity.Entity.__init__(self, position, Player.PLAYER_RADIUS, Player.PLAYER_MOVE_SPEED, True)
        self._can_shoot = True
        self._exhaust_frame = 0
        self._shield_timer = 0
        self._shield_duration = 0
        self._powerup_timer = 0
        self._shoot_cooldown = 0
        self._powerup_duration = 0
        self._active_powerup = Powerup.PowerupType.NONE
        shield_diameter = (Player.SHIELD_RADIUS + Player.SHIELD_RADIUS_THICKNESS) * 2
        self._shield_surface = pygame.Surface((shield_diameter, shield_diameter), pygame.SRCALPHA)
        self._shield_rect = pygame.Rect(0, 0, shield_diameter, shield_diameter)

        self._input = PlayerInput.PlayerInput.get_instance()
        self._sound_manager = SoundManager.SoundManager.get_instance()
        self._bullet_manager = BulletManager.BulletManager.get_instance()
        self._user_interface = UserInterface.UserInterface.get_instance()
        self._effect_manager = EffectManager.EffectManager.get_instance()
        self._powerup_manager = PowerupManager.PowerupManager.get_instance()

        self._sprite_exhaust: list[pygame.Surface] = []
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "Animations", "sprite_flame_01.png"))
        self._sprite_exhaust.append(pygame.transform.scale(sprite_temp, (25, 25)))
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "Animations", "sprite_flame_02.png"))
        self._sprite_exhaust.append(pygame.transform.scale(sprite_temp, (25, 25)))
        sprite_temp = pygame.image.load(os.path.join("Assets", "Images", "sprite_player.png"))
        self._sprite = pygame.transform.scale(sprite_temp, (self.PLAYER_RADIUS * 2, self.PLAYER_RADIUS * 2))

    def update(self) -> None:
        velocity = pygame.Vector2(0, 0)
        mouse_pos = self._input.get_mouse_position()
        self.look_at(mouse_pos.as_vector2())

        if self._shoot_cooldown > 0:
            self._shoot_cooldown -= 1
        else:
            self._can_shoot = True

        if self._input.get_mouse_down(0) and self._can_shoot:
            self._can_shoot = False
            if self._active_powerup == Powerup.PowerupType.RAPID_FIRE:
                self._shoot_cooldown = Player.PLAYER_SHOOT_COOLDOWN_POWERUP
            else:
                self._shoot_cooldown = Player.PLAYER_SHOOT_COOLDOWN

            did_shoot = self._bullet_manager.fire_player_bullet(self._position.clone(), mouse_pos.as_vector2() - self._position.as_vector2())
            if self._active_powerup == Powerup.PowerupType.SCATTER_SHOT:
                did_shoot = self.fire_scatter_shot(mouse_pos.as_vector2() - self._position.as_vector2())
            if did_shoot:
                self._sound_manager.play_sfx_laser()

        if self._input.get_key_down(self._input.INPUT_UP) or self._input.get_key_down(pygame.K_w):
            velocity.y = -1
        if self._input.get_key_down(self._input.INPUT_DOWN) or self._input.get_key_down(pygame.K_s):
            velocity.y = 1
        if self._input.get_key_down(self._input.INPUT_LEFT) or self._input.get_key_down(pygame.K_a):
            velocity.x = -1
        if self._input.get_key_down(self._input.INPUT_RIGHT) or self._input.get_key_down(pygame.K_d):
            velocity.x = 1

        if not velocity.magnitude() == 0.0:
            velocity = velocity.normalize()
            self._position.x += velocity.x * self._move_speed
            self._position.y += velocity.y * self._move_speed

        if self._shield_timer > 0:
            self._shield_timer -= 1
        if self._active_powerup != Powerup.PowerupType.NONE:
            self._powerup_timer -= 1
            if self._powerup_timer < 1:
                self._active_powerup = Powerup.PowerupType.NONE

        for powerup in self._powerup_manager.get_powerups():
            if not powerup.is_active():
                continue

            distance = self._position.distance_to(powerup.get_position().as_vector2())
            radius = self._radius + powerup.get_radius()
            if distance < float(radius):
                self.claim_powerup(powerup)

        self.constrain_to_view()

    def fire_scatter_shot(self, direction: pygame.Vector2) -> bool:
        left = direction.rotate(-Player.PLAYER_SHOOT_SCATTER_CONE)
        right = direction.rotate(Player.PLAYER_SHOOT_SCATTER_CONE)

        did_fire = [False, False]
        did_fire[0] = self._bullet_manager.fire_player_bullet(self._position.clone(), left)
        did_fire[1] = self._bullet_manager.fire_player_bullet(self._position.clone(), right)
        return any(did_fire)

    def claim_powerup(self, powerup: Powerup.Powerup) -> None:
        powerup.claim()

        if powerup.get_type() == Powerup.PowerupType.HEALTH_PACK:
            self.gain_health(powerup.HEALTH_BOOST)
            return

        if powerup.get_type() == Powerup.PowerupType.RAPID_FIRE:
            self._powerup_duration = powerup.RAPID_FIRE_TIMER
            self._powerup_timer = powerup.RAPID_FIRE_TIMER
            self._active_powerup = powerup.get_type()
            return

        if powerup.get_type() == Powerup.PowerupType.SCATTER_SHOT:
            self._powerup_duration = powerup.SCATTER_FIRE_TIMER
            self._powerup_timer = powerup.SCATTER_FIRE_TIMER
            self._active_powerup = powerup.get_type()

        if powerup.get_type() == Powerup.PowerupType.SHIELD:
            self._shield_duration = powerup.SHIELD_TIMER
            self._shield_timer = powerup.SHIELD_TIMER

    def get_shield_scale(self) -> float:
        if self._shield_duration == 0 or self._shield_timer == 0:
            return 0
        return self._shield_timer / self._shield_duration

    def take_damage(self, amount: int) -> None:
        if self._shield_timer > 0:
            self._shield_timer -= (amount * 5)
            return
        Entity.Entity.take_damage(self, amount)

    def die(self) -> None:
        self._effect_manager.create_player_explosion(self._position.clone(), 3)
        Entity.Entity.die(self)
        self._user_interface.set_game_state(UserInterface.GameState.GAME_OVER)

    def respawn(self, position: MyUtils.Point, move_speed: int = None) -> None:
        Entity.Entity.respawn(self, position, Player.PLAYER_MOVE_SPEED)
        self._can_shoot = True
        self._shoot_cooldown = 0
        self._powerup_timer = 0
        self._powerup_duration = 0
        self._active_powerup = Powerup.PowerupType.NONE
        self.look_at((self._position + MyUtils.Point(0, -1)).as_vector2())

    def render(self) -> None:
        if not self._alive:
            return

        self.render_exaust()
        render_sprite = pygame.transform.rotate(self._sprite, self._rotation)
        rect = render_sprite.get_rect()
        rect.x = self._position.x - (rect.width / 2)
        rect.y = self._position.y - (rect.height / 2)
        pygame.display.get_surface().blit(render_sprite, rect)

        if self._shield_timer > 0:
            self.render_shield()
        if self._active_powerup != Powerup.PowerupType.NONE:
            self.__render_powerup_timer()

    def render_exaust(self) -> None:
        self._exhaust_frame += 1
        if self._exhaust_frame >= len(self._sprite_exhaust):
            self._exhaust_frame = 0

        exhaust_offset = pygame.Vector2(0, 25).rotate(-self._rotation)
        render_sprite = pygame.transform.rotate(self._sprite_exhaust[self._exhaust_frame], self._rotation)
        rect = render_sprite.get_rect()
        rect.x = self._position.x - (rect.width / 2) + exhaust_offset.x
        rect.y = self._position.y - (rect.height / 2) + exhaust_offset.y
        pygame.display.get_surface().blit(render_sprite, rect)

    def render_shield(self) -> None:
        self._shield_surface.fill((0, 0, 0, 0))
        self._shield_rect.x = self._position.x - self._shield_rect.width / 2
        self._shield_rect.y = self._position.y - self._shield_rect.height / 2

        shield_circle = Player.SHIELD_RADIUS + (Player.SHIELD_RADIUS_THICKNESS * self.get_shield_scale())
        shield_center = (self._shield_rect.width / 2, self._shield_rect.height / 2)
        pygame.draw.circle(self._shield_surface, Player.SHIELD_COLOR, (shield_center[0], shield_center[0]), shield_circle)
        pygame.display.get_surface().blit(self._shield_surface, self._shield_rect)

    def __render_powerup_timer(self) -> None:
        powerup_scale = (self._powerup_timer / self._powerup_duration) if self._powerup_duration > 0 else 0
        offset = self._position + Player.HEALTH_BAR_OFFSET
        x_offset = offset.x - (Player.HEALTH_BAR_WIDTH / 2)
        y_offset = offset.y - (Player.HEALTH_BAR_HEIGHT / 2)
        rect = pygame.rect.Rect(
            pygame.Vector2(x_offset, y_offset),
            pygame.Vector2(Player.HEALTH_BAR_WIDTH * powerup_scale, Player.HEALTH_BAR_HEIGHT))
        pygame.draw.rect(pygame.display.get_surface(), (255, 255, 0), rect)
