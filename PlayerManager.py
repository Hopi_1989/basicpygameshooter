import MyUtils
import Player
from Constants import SCREEN_WIDTH, SCREEN_HEIGHT


class PlayerManager:
    _instance: 'PlayerManager' = None

    @staticmethod
    def get_instance() -> 'PlayerManager':
        if PlayerManager._instance is None:
            PlayerManager._instance = PlayerManager()
        return PlayerManager._instance

    PLAYER_START = MyUtils.Point(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)

    def __init__(self):
        self._player = Player.Player(MyUtils.Point(PlayerManager.PLAYER_START.x, PlayerManager.PLAYER_START.y))

    def get_player(self) -> Player:
        return self._player

    def get_health_scale(self) -> float:
        return self._player.get_health_scale()

    def get_shield_scale(self) -> float:
        return self._player.get_shield_scale()

    def update(self) -> None:
        self._player.update()

    def take_damage(self, amount: int) -> None:
        self._player.take_damage(amount)

    def respawn(self) -> None:
        self._player.respawn(MyUtils.Point(PlayerManager.PLAYER_START.x, PlayerManager.PLAYER_START.y))

    def render(self) -> None:
        self._player.render()
