import pygame
import typing
import MyUtils
from Constants import SCREEN_WIDTH, SCREEN_HEIGHT


class PlayerInput:
    _instance: 'PlayerInput' = None

    @staticmethod
    def get_instance() -> 'PlayerInput':
        if PlayerInput._instance is None:
            PlayerInput._instance = PlayerInput()

        return PlayerInput._instance

    INPUT_UP: int = pygame.K_UP
    INPUT_DOWN: int = pygame.K_DOWN
    INPUT_LEFT: int = pygame.K_LEFT
    INPUT_RIGHT: int = pygame.K_RIGHT
    INPUT_SPACE: int = pygame.K_SPACE
    INPUT_LSHIFT: int = pygame.K_LSHIFT
    INPUT_RSHIFT: int = pygame.K_RSHIFT
    INPUT_ESCAPE: int = pygame.K_ESCAPE
    CENTER: tuple[int, int] = (SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)

    def __init__(self):
        self._mouse_x, self._mouse_y = PlayerInput.CENTER
        self._mouse_buttons_down = (False, False, False)
        self._mouse_buttons_pressed = {
            0: False,
            1: False,
            2: False
        }
        self._keys_pressed = {
            self.INPUT_UP: False,
            self.INPUT_DOWN: False,
            self.INPUT_LEFT: False,
            self.INPUT_RIGHT: False,
            self.INPUT_SPACE: False,
            self.INPUT_LSHIFT: False,
            self.INPUT_RSHIFT: False,
            self.INPUT_ESCAPE: False
        }
        self._keys_down: typing.Sequence[bool] = []

    def update(self) -> None:
        mouse_change = pygame.mouse.get_rel()
        pygame.mouse.set_pos(PlayerInput.CENTER)

        self._mouse_x += mouse_change[0]
        self._mouse_y += mouse_change[1]

        if self._mouse_x < 0:
            self._mouse_x = 0
        if self._mouse_x > SCREEN_WIDTH:
            self._mouse_x = SCREEN_WIDTH
        if self._mouse_y < 0:
            self._mouse_y = 0
        if self._mouse_y > SCREEN_HEIGHT:
            self._mouse_y = SCREEN_HEIGHT

        self._mouse_buttons_down = pygame.mouse.get_pressed()
        for button_index in range(len(self._mouse_buttons_down)):
            if not self._mouse_buttons_down[button_index]:
                self._mouse_buttons_pressed[button_index] = False

        self._keys_down = pygame.key.get_pressed()
        for key in self._keys_pressed.keys():
            if not self._keys_down[key]:
                self._keys_pressed[key] = False

    def get_key_down(self, key: int) -> bool:
        if not self._check_update():
            return False
        return self._keys_down[key]

    def get_key_pressed(self, key: int) -> bool:
        if not self._check_update() or self._keys_pressed[key] or not self._keys_down[key]:
            return False
        self._keys_pressed[key] = True
        return True

    def get_mouse_position(self) -> MyUtils.Point:
        return MyUtils.Point(float(self._mouse_x), float(self._mouse_y))

    def get_mouse_x(self) -> int:
        return self._mouse_x

    def get_mouse_y(self) -> int:
        return self._mouse_y

    def get_mouse_down(self, button: int = 0) -> bool:
        return self._mouse_buttons_down[button]

    def get_mouse_pressed(self, button: int = 0) -> bool:
        if self._mouse_buttons_pressed[button] or not self._mouse_buttons_down[button]:
            return False
        self._mouse_buttons_pressed[button] = True
        return True

    def _check_update(self) -> bool:
        if not self._keys_down:
            return False
        return True
