import pygame
import MyUtils
import Bullet
import EnemyManager
import PlayerManager


class BulletManager:
    _instance: 'BulletManager' = None

    @staticmethod
    def get_instance() -> 'BulletManager':
        if BulletManager._instance is None:
            BulletManager._instance = BulletManager()
        return BulletManager._instance

    MAX_BULLETS: int = 15

    def __init__(self):
        self._player_bullets: list[Bullet.Bullet] = []
        self._enemy_bullets: list[Bullet.Bullet] = []

    def fire_player_bullet(self, position: MyUtils.Point, direction: pygame.Vector2) -> bool:
        return self._fire_bullet(position, direction, True, False)

    def fire_enemy_bullet(self, position: MyUtils.Point, direction: pygame.Vector2, boss_bullet: bool = False) -> None:
        self._fire_bullet(position, direction, False, boss_bullet)

    def _fire_bullet(self, position: MyUtils.Point, direction: pygame.Vector2, player_bullet: bool, boss_bullet: bool) -> bool:
        bullet_list = self._player_bullets if player_bullet else self._enemy_bullets
        if len(bullet_list) < BulletManager.MAX_BULLETS:
            bullet_list.append(Bullet.Bullet(position, direction, player_bullet, boss_bullet))
            return True

        for bullet in bullet_list:
            if bullet.is_active():
                continue

            bullet.reactivate(position, direction, player_bullet, boss_bullet)
            return True
        return False

    def update(self) -> None:
        for player_bullet in self._player_bullets:
            player_bullet.update_multiple_targets(EnemyManager.EnemyManager.get_instance().get_enemies_list())

        for enemy_bullet in self._enemy_bullets:
            enemy_bullet.update_single_target(PlayerManager.PlayerManager.get_instance().get_player())

    def render(self) -> None:
        for player_bullet in self._player_bullets:
            player_bullet.render()

        for enemy_bullet in self._enemy_bullets:
            enemy_bullet.render()

    def reset(self) -> None:
        self.__init__()
