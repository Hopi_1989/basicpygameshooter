import os
import enum
import pygame
import MyUtils


class EffectType(enum.Enum):
    EXPLOSION_ENEMY = 1
    EXPLOSION_PLAYER = 2


class Effect:
    EXPLOSION_ENEMY: list[pygame.Surface] = []
    EXPLOSION_PLAYER: list[pygame.Surface] = []

    def __init__(self, position: MyUtils.Point, effect_type: EffectType, scale: float):
        self._active = True
        self._scale = scale
        self._current_frame = 0
        self._effect_type = effect_type
        self._position = position
        self._rect = self.get_rect(scale)
        self._rect.x = self._position.x - (self._rect.width / 2)
        self._rect.y = self._position.y - (self._rect.height / 2)

    def is_active(self) -> bool:
        return self._active

    def update(self) -> None:
        self._current_frame += 1
        if self._current_frame >= self.get_effect_duration():
            self._active = False

    def render(self) -> None:
        if not self._active:
            return

        if self._scale == 1.0:
            pygame.display.get_surface().blit(self.get_effect_sprite(self._current_frame), self._rect)
            return

        scaled_sprite = pygame.transform.scale(
            self.get_effect_sprite(self._current_frame),
            (self._rect.width, self._rect.height))
        pygame.display.get_surface().blit(scaled_sprite, self._rect)

    def get_effect_duration(self) -> int:
        if self._effect_type == EffectType.EXPLOSION_ENEMY:
            return len(Effect.EXPLOSION_ENEMY)
        if self._effect_type == EffectType.EXPLOSION_PLAYER:
            return len(Effect.EXPLOSION_PLAYER)
        return 0

    def get_effect_sprite(self, frame: int) -> pygame.Surface | None:
        if self._effect_type == EffectType.EXPLOSION_ENEMY:
            return Effect.EXPLOSION_ENEMY[frame]
        if self._effect_type == EffectType.EXPLOSION_PLAYER:
            return Effect.EXPLOSION_PLAYER[frame]
        return None

    def get_rect(self, scale: float) -> pygame.Rect | None:
        rect: pygame.Rect | None = None
        if self._effect_type == EffectType.EXPLOSION_ENEMY:
            rect = Effect.EXPLOSION_ENEMY[0].get_rect()
        if self._effect_type == EffectType.EXPLOSION_PLAYER:
            rect = Effect.EXPLOSION_PLAYER[0].get_rect()

        rect.width *= scale
        rect.height *= scale
        return rect

    @staticmethod
    def load_effects() -> None:
        for i in range(25):
            Effect.EXPLOSION_ENEMY.append(pygame.image.load(os.path.join("Assets", "Images", "Animations", "Explosion", f"Anim_Explosion_{i + 1}.png")))
        for i in range(30):
            Effect.EXPLOSION_PLAYER.append(pygame.image.load(os.path.join("Assets", "Images", "Animations", "Explosion2", f"Anim_Explosion_{i + 1}.png")))