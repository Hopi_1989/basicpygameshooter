import pygame
import MyUtils
from Constants import SCREEN_WIDTH, SCREEN_HEIGHT


class Entity:
    START_HEALTH = 100
    HEALTH_BAR_WIDTH = 50
    HEALTH_BAR_HEIGHT = 6
    HEALTH_BAR_OFFSET = MyUtils.Point(0, -30)

    def __init__(self, position: MyUtils.Point, radius: int, move_speed: int, alive: bool):
        self._alive = alive
        self._rotation = 0.0
        self._radius = radius
        self._position = position
        self._move_speed = move_speed
        self._health = Entity.START_HEALTH

    def is_alive(self) -> bool:
        return self._alive

    def get_position(self) -> MyUtils.Point:
        return self._position

    def get_radius(self) -> int:
        return self._radius

    def get_health(self) -> int:
        return self._health

    def get_health_scale(self) -> float:
        return float(self._health) / float(Entity.START_HEALTH)

    def look_at(self, point: pygame.Vector2) -> None:
        direction = point - self._position.as_vector2()
        self._rotation = direction.angle_to(pygame.Vector2(0, -1))

    def constrain_to_view(self, padding: MyUtils.Point = MyUtils.Point()) -> None:
        """
        Constraings the entity to the viewable part of the screen

        :param padding: how far Entities can go offscreen
        """
        if self._position.x - self._radius < 0 - padding.x:
            self._position.x = self._radius - padding.x
        if self._position.x + self._radius > SCREEN_WIDTH + padding.x:
            self._position.x = (SCREEN_WIDTH + padding.x) - self._radius
        if self._position.y - self._radius < 0 - padding.y:
            self._position.y = self._radius - padding.y
        if self._position.y + self._radius > SCREEN_HEIGHT + padding.y:
            self._position.y = (SCREEN_HEIGHT + padding.y) - self._radius

    def take_damage(self, amount: int) -> None:
        self._health -= amount
        if self._health < 1:
            self.die()

    def gain_health(self, amount: int) -> None:
        self._health += amount
        if self._health > Entity.START_HEALTH:
            self._health = Entity.START_HEALTH

    def die(self) -> None:
        self._alive = False
        self._position.x = 0
        self._position.y = 0
        self._move_speed = 0

    def respawn(self, position: MyUtils.Point, move_speed: int = 1) -> None:
        self._position = position
        self._move_speed = move_speed
        self._alive = True
        self._health = Entity.START_HEALTH

    def render(self) -> None:
        """
        Base render function. This function is called to render a default health bar on Entity objects
        """
        if self._alive:
            # red background
            x_offset = self._position.x + Entity.HEALTH_BAR_OFFSET.x - (Entity.HEALTH_BAR_WIDTH / 2)
            y_offset = self._position.y + Entity.HEALTH_BAR_OFFSET.y - (Entity.HEALTH_BAR_HEIGHT / 2)
            rect = pygame.rect.Rect(
                pygame.Vector2(x_offset, y_offset),
                pygame.Vector2(Entity.HEALTH_BAR_WIDTH, Entity.HEALTH_BAR_HEIGHT))
            pygame.draw.rect(pygame.display.get_surface(), (255, 0, 0), rect)

            # green foreground
            rect.width = float(Entity.HEALTH_BAR_WIDTH) * self.get_health_scale()
            pygame.draw.rect(pygame.display.get_surface(), (0, 255, 0), rect)
