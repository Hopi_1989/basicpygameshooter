import os
import pygame


class SoundManager:
    _instance: 'SoundManager' = None

    @staticmethod
    def get_instance() -> 'SoundManager':
        if SoundManager._instance is None:
            SoundManager._instance = SoundManager()
        return SoundManager._instance

    def __init__(self):
        pygame.mixer.init()

        self._BG_LEVEL = pygame.mixer.Sound(os.path.join("Assets", "Sounds", "bg_music4.ogg"))
        self._BG_BOSS = pygame.mixer.Sound(os.path.join("Assets", "Sounds", "bg_boss.ogg"))
        self._SFX_LASER = pygame.mixer.Sound(os.path.join("Assets", "Sounds", "sfx_laser1.ogg"))
        self._SFX_LASER.set_volume(0.5)
        self._CHANNEL_BGM_LEVEL = pygame.mixer.Channel(1)
        self._CHANNEL_BGM_BOSS = pygame.mixer.Channel(2)

    def play_bgm_level(self) -> None:
        self._CHANNEL_BGM_LEVEL.play(self._BG_LEVEL, -1, fade_ms=1000)

    def play_bgm_boss(self) -> None:
        self._CHANNEL_BGM_BOSS.play(self._BG_BOSS, -1, fade_ms=1000)

    def check_bgm_level(self) -> bool:
        return self._CHANNEL_BGM_LEVEL.get_busy()

    def check_bgm_boss(self) -> bool:
        return self._CHANNEL_BGM_BOSS.get_busy()

    def stop_bgm_level(self) -> None:
        self._CHANNEL_BGM_LEVEL.fadeout(1000)

    def stop_bgm_boss(self) -> None:
        self._CHANNEL_BGM_BOSS.fadeout(1000)

    def play_sfx_laser(self) -> None:
        self._SFX_LASER.play()
