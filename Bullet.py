import pygame

import Entity
import MyUtils
from Constants import SCREEN_WIDTH, SCREEN_HEIGHT


class Bullet:
    BULLET_COLOR: tuple[int, int, int] = (255, 255, 0)
    BULLET_COLOR_ENEMY: tuple[int, int, int] = (255, 0, 0)

    BULLET_SPEED: int = 20
    BULLET_SPEED_ENEMY: int = 10
    BULLET_SPEED_BOSS: int = 15

    BULLET_RADIUS: int = 5
    BULLET_RADIUS_BOSS: int = 25

    BULLET_DAMAGE_BASE: int = 25
    BULLET_DAMAGE_ENEMY: int = 10
    BULLET_DAMAGE_BOSS: int = 20

    def __init__(self, position: MyUtils.Point, direction: pygame.Vector2, player_bullet: bool = True, boss_bullet: bool = False):
        self._active = True
        self._position = position
        self._player_bullet = player_bullet
        self._move_direction = direction.normalize()
        self._radius = Bullet.BULLET_RADIUS_BOSS if boss_bullet else Bullet.BULLET_RADIUS
        self._damage = Bullet._get_damage_value(player_bullet, boss_bullet)
        self._speed = Bullet._get_speed(player_bullet, boss_bullet)

    def is_active(self) -> bool:
        return self._active

    def update_multiple_targets(self, targets: list[Entity.Entity]) -> None:
        if not self._active:
            return

        self._update_bullet()
        for target in targets:
            if not target.is_alive():
                continue

            if self._collided_with_target(target):
                target.take_damage(self._damage)
                self.deactivate()

    def update_single_target(self, target: Entity.Entity) -> None:
        if not self._active or not target.is_alive():
            return

        self._update_bullet()
        if self._collided_with_target(target):
            target.take_damage(self._damage)
            self.deactivate()

    def deactivate(self) -> None:
        self._active = False
        self._position.x = 0
        self._position.y = 0

    def reactivate(self, position: MyUtils.Point, direction: pygame.Vector2, player_bullet: bool = True, boss_bullet: bool = False) -> None:
        self.__init__(position, direction, player_bullet, boss_bullet)

    @staticmethod
    def _get_speed(player_bullet: bool, boss_bullet: bool) -> int:
        if player_bullet:
            return Bullet.BULLET_SPEED

        return Bullet.BULLET_SPEED_BOSS if boss_bullet else Bullet.BULLET_SPEED_ENEMY

    @staticmethod
    def _get_damage_value(player_bullet: bool, boss_bullet: bool) -> int:
        if player_bullet:
            return Bullet.BULLET_DAMAGE_BASE

        return Bullet.BULLET_DAMAGE_BOSS if boss_bullet else Bullet.BULLET_DAMAGE_ENEMY

    def _update_bullet(self) -> None:
        self._position.x += self._move_direction.x * self._speed
        self._position.y += self._move_direction.y * self._speed

        bullet_padding = 100  # distance off-screen bullets are allowed
        if self._position.x < -bullet_padding or self._position.x > SCREEN_WIDTH + bullet_padding:
            self.deactivate()
        if self._position.y < -bullet_padding or self._position.y > SCREEN_HEIGHT + bullet_padding:
            self.deactivate()

    def _collided_with_target(self, target: Entity.Entity) -> bool:
        radius = self._radius + target.get_radius()
        if self._position.distance_to(target.get_position().as_vector2()) < radius:
            return True

        return False

    def render(self) -> None:
        if not self._active:
            return

        bullet_color = Bullet.BULLET_COLOR if self._player_bullet else Bullet.BULLET_COLOR_ENEMY
        pygame.draw.circle(pygame.display.get_surface(), bullet_color, (self._position.x, self._position.y), self._radius)
