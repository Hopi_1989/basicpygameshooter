import math
import random
import pygame
import Enemy
import MyUtils
from Constants import SCREEN_HEIGHT, SCREEN_WIDTH


class EnemyManager:
    _instance: 'EnemyManager' = None

    @staticmethod
    def get_instance() -> 'EnemyManager':
        if EnemyManager._instance is None:
            EnemyManager._instance = EnemyManager()
        return EnemyManager._instance

    MAX_NUM_ENEMIES = 10
    MIN_MOVE_SPEED = 1
    MAX_MOVE_SPEED = 5
    MAX_SPAWN_INTERVAL = 75
    MIN_SPAWN_INTERVAL = 15
    BOSS_ROUND_INTERVAL = 5  # every X rounds is a boss round
    
    def __init__(self):
        Enemy.Enemy.load_sprites()

        self._boss_round = False
        self._spawn_boss = False
        self._enemies: list[Enemy.Enemy] = []
        self._boss = Enemy.Enemy(MyUtils.Point(), alive=False)

        self._spawn_ticks = 0
        self._current_round = 1
        self._enemies_spawned = 0
        self._enemies_destroyed = 0
        self._spawn_quota = EnemyManager.MAX_NUM_ENEMIES
        self._base_move_speed = EnemyManager.MIN_MOVE_SPEED
        self._spawn_radius = math.sqrt((SCREEN_WIDTH / 2) ** 2 + (SCREEN_HEIGHT / 2) ** 2) + 50
        self._spawn_interval = EnemyManager.MAX_SPAWN_INTERVAL

        # prepopulate a list of enemies, so we aren't allocating memory during gameplay
        for i in range(EnemyManager.MAX_NUM_ENEMIES):
            self._enemies.append(Enemy.Enemy(MyUtils.Point(0, 0), 0, False))

    def set_round(self, current_round: int) -> None:
        t_current_round = self._current_round
        self._spawn_quota = 0
        self._spawn_ticks = 0
        self._enemies_spawned = 0
        self._enemies_destroyed = 0
        self._spawn_quota = EnemyManager.MAX_NUM_ENEMIES + ((current_round - 1) * 2)
        self._boss_round = (current_round % EnemyManager.BOSS_ROUND_INTERVAL) == 0
        self._spawn_boss = self._boss_round
        self._current_round = current_round

        for enemy in self._enemies:
            enemy.reset()
        if self._boss_round:
            return

        if t_current_round < current_round:
            self._base_move_speed += 1
            self._spawn_interval -= 5 if (current_round > t_current_round) else 0
            if self._base_move_speed > EnemyManager.MAX_MOVE_SPEED:
                self._base_move_speed = EnemyManager.MAX_MOVE_SPEED
            if self._spawn_interval < EnemyManager.MIN_SPAWN_INTERVAL:
                self._spawn_interval = EnemyManager.MIN_SPAWN_INTERVAL

    def spawn_enemy(self, position: MyUtils.Point, boss_phase: bool = False) -> None:
        for enemy in self._enemies:
            if enemy.is_alive():
                continue

            self._enemies_spawned += 1
            enemy_type = Enemy.EnemyType.AGGRO
            move_speed = self._get_random_speed()
            if self._enemies_spawned % 5 == 0:
                enemy_type = Enemy.EnemyType.PROJECTILE
                move_speed = EnemyManager.MIN_MOVE_SPEED

            if boss_phase:
                move_speed -= 2  # spawn slightly slower enemies during boss phase
            enemy.respawn_type(position, max(move_speed, 1), enemy_type)
            return

    def spawn_boss(self, position: MyUtils.Point) -> None:
        move_speed = self._base_move_speed - 3
        self._boss.respawn_type(position, max(move_speed, 1), Enemy.EnemyType.BOSS)
        self._spawn_boss = False
        return

    def get_spawn_position(self) -> MyUtils.Point:
        spawn_direction = pygame.Vector2(random.randrange(-100, 100, 3), random.randrange(-100, 100, 3))
        spawn_direction = spawn_direction.normalize()
        return MyUtils.Point(
            (spawn_direction.x * self._spawn_radius) + float(SCREEN_WIDTH) / 2.0,
            (spawn_direction.y * self._spawn_radius) + float(SCREEN_HEIGHT) / 2.0)

    def update(self) -> None:
        for enemy in self._enemies:
            enemy.update()
            if enemy.was_destroyed():
                self._enemies_destroyed += 1

        if self._boss_round:
            if self._spawn_boss:
                self.spawn_boss(self.get_spawn_position())

            self._boss.update()
            self._boss.set_shield(any([enemy for enemy in self._enemies if enemy.is_alive()]))
            if self._boss.spawn_minions():
                for i in range(5):
                    self.spawn_enemy(self.get_spawn_position(), True)
            return

        if self._enemies_spawned == self._spawn_quota:
            return
        if self._spawn_ticks > self._spawn_interval:
            self._spawn_ticks = 0
            self.spawn_enemy(self.get_spawn_position())

        self._spawn_ticks += 1

    def all_enemies_destroyed(self) -> bool:
        if self._boss_round:
            return not self._boss.is_alive()
        return self._enemies_destroyed == self._spawn_quota

    def render(self) -> None:
        for enemy in self._enemies:
            enemy.render()

        if self._boss_round:
            self._boss.render()

    def get_enemies_list(self) -> list[Enemy]:
        enemy_list = list(self._enemies)
        if self._boss_round:
            enemy_list.append(self._boss)
        return enemy_list

    def is_boss_round(self) -> bool:
        return self._boss_round

    def get_boss_health_scale(self) -> float:
        if not self.is_boss_round():
            return 0.0
        return self._boss.get_health_scale()

    def reset(self) -> None:
        self.__init__()

    def _get_random_speed(self) -> int:
        return random.randrange(self._base_move_speed, self._base_move_speed + 2)
