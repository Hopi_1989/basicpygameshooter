import random
import MyUtils
import Powerup


class PowerupManager:
    _instance: 'PowerupManager' = None

    @staticmethod
    def get_instance() -> 'PowerupManager':
        if PowerupManager._instance is None:
            PowerupManager._instance = PowerupManager()
        return PowerupManager._instance

    SPAWN_FREQUENCY: int = 15  # after this number of enemies die, spawn a powerup

    def __init__(self):
        self._powerups: list[Powerup.Powerup] = []
        self._enemies_destroyed = 0
        Powerup.Powerup.load_sprites()

    def update(self) -> None:
        self._powerups = [powerup for powerup in self._powerups if powerup.is_active()]
        for powerup in self._powerups:
            powerup.update()

    def enemy_destroyed(self, position: MyUtils.Point = MyUtils.Point) -> None:
        self._enemies_destroyed += 1
        if self._enemies_destroyed > PowerupManager.SPAWN_FREQUENCY - 1:
            self.spawn_random_powerup(position)
            self._enemies_destroyed = 0

    def spawn_random_powerup(self, position: MyUtils.Point) -> None:
        powerup_type = random.randint(1, len(Powerup.PowerupType) - 1)
        self._spawn_powerup(Powerup.PowerupType(powerup_type), position)

    def spawn_health_pack(self, position: MyUtils.Point) -> None:
        self._spawn_powerup(Powerup.PowerupType.HEALTH_PACK, position)

    def spawn_rapid_fire(self, position: MyUtils.Point) -> None:
        self._spawn_powerup(Powerup.PowerupType.RAPID_FIRE, position)

    def spawn_scatter_fire(self, position: MyUtils.Point) -> None:
        self._spawn_powerup(Powerup.PowerupType.SCATTER_SHOT, position)

    def spawn_shield(self, position: MyUtils.Point) -> None:
        self._spawn_powerup(Powerup.PowerupType.SHIELD, position)

    def _spawn_powerup(self, powerup_type: Powerup.PowerupType, position: MyUtils.Point) -> None:
        new_powerup = Powerup.Powerup(position, Powerup.PowerupType(powerup_type))
        self._powerups.append(new_powerup)

    def get_powerups(self) -> list[Powerup.Powerup]:
        return self._powerups

    def reset(self) -> None:
        self._powerups.clear()
        self._enemies_destroyed = 0

    def render(self) -> None:
        for powerup in self._powerups:
            powerup.render()
