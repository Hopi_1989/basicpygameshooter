import os
import pygame
import enum
import MyUtils
from Constants import FRAME_RATE


class PowerupType(enum.Enum):
    NONE = 0
    HEALTH_PACK = 1
    RAPID_FIRE = 2
    SCATTER_SHOT = 3
    SHIELD = 4


class Powerup:
    SPRITE_HEALTH_PACK: pygame.Surface = None
    SPRITE_RAPID_FIRE: pygame.Surface = None
    SPRITE_SCATTER_SHOT: pygame.Surface = None
    SPRITE_SHIELD: pygame.Surface = None

    RADIUS: int = 15
    HEALTH_BOOST = 25
    RAPID_FIRE_TIMER = 10 * FRAME_RATE
    SCATTER_FIRE_TIMER = 20 * FRAME_RATE
    SHIELD_TIMER = 25 * FRAME_RATE

    def __init__(self, position: MyUtils.Point, powerup_type: PowerupType):
        self._active = True
        self._powerup_type = powerup_type
        self._position = MyUtils.Point(position.x, position.y)

    def is_active(self) -> bool:
        return self._active

    def get_radius(self) -> int:
        return self.RADIUS

    def get_type(self) -> PowerupType:
        return self._powerup_type

    def get_position(self) -> MyUtils.Point:
        return self._position

    def update(self) -> None:
        # if any future powerups have animations, behaviors etc., we can update them here
        if not self._active:
            return

    def claim(self) -> None:
        self._active = False

    def render(self) -> None:
        sprite = self.get_sprite()
        if not self._active or sprite is None:
            return

        rect = sprite.get_rect()
        rect.x = self._position.x - (rect.width / 2)
        rect.y = self._position.y - (rect.height / 2)
        pygame.display.get_surface().blit(sprite, rect)

    def get_sprite(self) -> pygame.Surface | None:
        if self._powerup_type == PowerupType.HEALTH_PACK:
            return Powerup.SPRITE_HEALTH_PACK
        if self._powerup_type == PowerupType.RAPID_FIRE:
            return Powerup.SPRITE_RAPID_FIRE
        if self._powerup_type == PowerupType.SCATTER_SHOT:
            return Powerup.SPRITE_SCATTER_SHOT
        if self._powerup_type == PowerupType.SHIELD:
            return Powerup.SPRITE_SHIELD

        return None

    @staticmethod
    def load_sprites() -> None:
        Powerup.SPRITE_HEALTH_PACK = pygame.transform.scale(
            pygame.image.load(os.path.join("Assets", "Images", "sprite_health.png")), (Powerup.RADIUS * 2, Powerup.RADIUS * 2))

        Powerup.SPRITE_RAPID_FIRE = pygame.transform.scale(
            pygame.image.load(os.path.join("Assets", "Images", "sprite_rapid_fire.png")), (Powerup.RADIUS * 2, Powerup.RADIUS * 2))

        Powerup.SPRITE_SCATTER_SHOT = pygame.transform.scale(
            pygame.image.load(os.path.join("Assets", "Images", "sprite_scatter_fire.png")), (Powerup.RADIUS * 2, Powerup.RADIUS * 2))

        Powerup.SPRITE_SHIELD = pygame.transform.scale(
            pygame.image.load(os.path.join("Assets", "Images", "sprite_shield.png")), (Powerup.RADIUS * 2, Powerup.RADIUS * 2))
