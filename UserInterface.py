import os
import enum
import pygame
import MyUtils
import PlayerInput
import Scoreboard
import EnemyManager
import PlayerManager
import SoundManager
from Constants import SCREEN_WIDTH, SCREEN_HEIGHT, GAME_FONT


class GameState(enum.Enum):
    INTRO = 1
    UNPAUSED = 2
    PAUSED = 3
    GAME_OVER = 4
    RESET = 5
    EXIT = 6
    ROUND_CLEAR = 7


class UIText:
    INTRO = "Press <Space> to Play"
    CONTROLS_1 = "<W, A, S, D>, or Arrow, Keys to move"
    CONTROLS_2 = "Left Click to shoot"
    CONTROLS_3 = "Left or Right <Shift> to pause"
    PAUSED = "PAUSED"
    UNPAUSED = "Left or Right <Shift> to unpause"
    EXIT = "<Escape> to exit"
    GAMEOVER = "GAME OVER"
    RESET = "<Space> to restart"
    ROUND_CLEAR = "Round Clear"
    ROUND = "Round: "
    BOSS_HEALTH = "Boss Health"
    BOSS_ROUND = "Boss Round"
    YOUR_SCORE = "Your score was: "

    @staticmethod
    def round(current_round: int) -> str:
        return UIText.ROUND + str(current_round)

    @staticmethod
    def score(score: int) -> str:
        return UIText.YOUR_SCORE + str(score)


class Overlay:
    def __init__(self, overlay_font: pygame.font.Font, text_color: tuple[int, int, int], bg_color: tuple[int, int, int] | None = None):
        self._font = overlay_font
        self._text_color = text_color
        self._bg_color = bg_color

    def render(self, text: str, position: MyUtils.Point = MyUtils.Point(0, 0)) -> None:
        overlay = self._font.render(text, True, self._text_color, self._bg_color)
        overlay_rect = overlay.get_rect()
        overlay_rect.x = position.x - (overlay_rect.width / 2.0)
        overlay_rect.y = position.y - (overlay_rect.height / 2.0)
        pygame.display.get_surface().blit(overlay, overlay_rect)


class UserInterface:
    _instance: 'UserInterface' = None

    @staticmethod
    def get_instance() -> 'UserInterface':
        if UserInterface._instance is None:
            UserInterface._instance = UserInterface()
        return UserInterface._instance

    def __init__(self):
        self._input = PlayerInput.PlayerInput.get_instance()
        self._sound_manager = SoundManager.SoundManager.get_instance()
        self._enemy_manager = EnemyManager.EnemyManager.get_instance()
        self._scoreboard = Scoreboard.Scoreboard.get_instance()

        self._HEALTHBAR_POSITION_BOSS = MyUtils.Point(SCREEN_WIDTH * 0.25, SCREEN_HEIGHT - 50)
        self._HEALTHBAR_SIZE_BOSS = MyUtils.Point(SCREEN_WIDTH * .5, 15)
        self._HEALTHBAR_POSITION = MyUtils.Point(25, 25)
        self._HEALTHBAR_SIZE = MyUtils.Point(250, 20)
        self._ROUND_TRANSITION_TIME = 180

        self._transition_time = 0
        self._current_round = 0
        self._game_state = GameState.UNPAUSED
        self._overlay_position = MyUtils.Point(SCREEN_WIDTH / 2, SCREEN_HEIGHT * 0.4)
        self._overlay_primary = Overlay(pygame.font.Font(GAME_FONT, 36), (255, 255, 255))
        self._overlay_secondary = Overlay(pygame.font.Font(GAME_FONT, 24), (255, 255, 255))
        self._overlay_boss = Overlay(pygame.font.Font(GAME_FONT, 16), (255, 255, 255))

        temp_sprite = pygame.image.load(os.path.join("Assets", "Images", "sprite_crosshair.png"))
        self._crosshair = pygame.transform.scale(temp_sprite, (45, 45))

    def get_game_state(self) -> GameState:
        return self._game_state

    def set_game_state(self, game_state: GameState) -> None:
        self._game_state = game_state

    def set_round(self, current_round: int):
        self._current_round = current_round

    def reset(self) -> None:
        self._current_round = 0
        self._scoreboard.reset_score()
        self.set_game_state(GameState.INTRO)

    def update(self) -> None:
        if self.get_game_state() == GameState.INTRO:
            if self._sound_manager.check_bgm_boss():
                self._sound_manager.stop_bgm_boss()

            if not self._sound_manager.check_bgm_level():
                self._sound_manager.play_bgm_level()

            if self._input.get_key_pressed(self._input.INPUT_SPACE):
                self.set_game_state(GameState.UNPAUSED)
                return

            if self._input.get_key_pressed(self._input.INPUT_ESCAPE):
                self.set_game_state(GameState.EXIT)
                return

        if self.get_game_state() == GameState.GAME_OVER:
            if self._input.get_key_pressed(self._input.INPUT_SPACE):
                self.set_game_state(GameState.RESET)
                return

            if self._input.get_key_pressed(self._input.INPUT_ESCAPE):
                self.set_game_state(GameState.EXIT)
                return

        if self.get_game_state() == GameState.PAUSED:
            if self._input.get_key_pressed(self._input.INPUT_LSHIFT) or self._input.get_key_pressed(self._input.INPUT_RSHIFT):
                self.set_game_state(GameState.UNPAUSED)
                return

            if self._input.get_key_pressed(self._input.INPUT_ESCAPE):
                self.set_game_state(GameState.EXIT)
                return

        if self.get_game_state() == GameState.UNPAUSED:
            if self._input.get_key_pressed(self._input.INPUT_LSHIFT) or self._input.get_key_pressed(self._input.INPUT_RSHIFT):
                self._game_state = GameState.PAUSED
                return

        if self.get_game_state() == GameState.ROUND_CLEAR:
            if self._transition_time == 0:
                self._transition_time = self._ROUND_TRANSITION_TIME
                return

            if self._transition_time < 75:
                if self._enemy_manager.is_boss_round():
                    self._sound_manager.stop_bgm_level()
                else:
                    self._sound_manager.stop_bgm_boss()

            self._transition_time -= 1
            if self._transition_time < 1:
                self._transition_time = 0
                self.set_game_state(GameState.UNPAUSED)

                if self._enemy_manager.is_boss_round():
                    self._sound_manager.play_bgm_boss()
                    return

                if not self._sound_manager.check_bgm_level():
                    self._sound_manager.play_bgm_level()

    def get_score(self) -> int:
        return self._scoreboard.get_score()

    def increment_score(self, amount: int = 1) -> None:
        self._scoreboard.increment_score(amount)

    def render(self) -> None:
        self.render_crosshair()
        round_text = UIText.BOSS_ROUND if self._enemy_manager.is_boss_round() else UIText.round(self._current_round)

        if self.get_game_state() == GameState.INTRO:
            self._overlay_primary.render(UIText.INTRO, self._overlay_position)
            self._overlay_secondary.render(UIText.CONTROLS_1, self._overlay_position + MyUtils.Point(0, 150))
            self._overlay_secondary.render(UIText.CONTROLS_2, self._overlay_position + MyUtils.Point(0, 200))
            self._overlay_secondary.render(UIText.CONTROLS_3, self._overlay_position + MyUtils.Point(0, 250))
            self._overlay_secondary.render(UIText.EXIT, self._overlay_position + MyUtils.Point(0, 300))
            return

        if self.get_game_state() == GameState.UNPAUSED:
            self._scoreboard.render()
            self.render_healthbar()
            self.render_boss_healthbar()
            self._overlay_secondary.render(round_text, MyUtils.Point(SCREEN_WIDTH / 2, 35))
            return

        if self.get_game_state() == GameState.PAUSED:
            self._scoreboard.render()
            self.render_healthbar()
            self.render_boss_healthbar()
            self._overlay_primary.render(UIText.PAUSED, self._overlay_position)
            self._overlay_secondary.render(UIText.UNPAUSED, self._overlay_position + MyUtils.Point(0, 100))
            self._overlay_secondary.render(UIText.EXIT, self._overlay_position + MyUtils.Point(0, 150))
            self._overlay_secondary.render(round_text, MyUtils.Point(SCREEN_WIDTH / 2, 35))
            return

        if self.get_game_state() == GameState.GAME_OVER:
            self.render_boss_healthbar()
            self._overlay_primary.render(UIText.GAMEOVER, self._overlay_position)
            score_text = UIText.score(self._scoreboard.get_score())
            self._overlay_secondary.render(score_text, self._overlay_position + MyUtils.Point(0, 50))
            self._overlay_secondary.render(UIText.RESET, self._overlay_position + MyUtils.Point(0, 100))
            self._overlay_secondary.render(UIText.EXIT, self._overlay_position + MyUtils.Point(0, 150))
            self._overlay_secondary.render(round_text, MyUtils.Point(SCREEN_WIDTH / 2, 35))
            return

        if self.get_game_state() == GameState.ROUND_CLEAR:
            self._scoreboard.render()
            self._overlay_primary.render(UIText.ROUND_CLEAR, self._overlay_position)
            self.render_healthbar()
            self.render_crosshair()
            return

    def render_boss_healthbar(self):
        if not self._enemy_manager.is_boss_round():
            return

        self._overlay_boss.render(UIText.BOSS_HEALTH, MyUtils.Point(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 65))
        # red background
        rect = pygame.Rect(
            (self._HEALTHBAR_POSITION_BOSS.x, self._HEALTHBAR_POSITION_BOSS.y),
            (self._HEALTHBAR_SIZE_BOSS.x, self._HEALTHBAR_SIZE_BOSS.y))
        pygame.draw.rect(pygame.display.get_surface(), (255, 0, 0), rect)

        # green foreground
        rect.width *= self._enemy_manager.get_boss_health_scale()
        pygame.draw.rect(pygame.display.get_surface(), (0, 255, 0), rect)

    def render_healthbar(self) -> None:
        # red background
        rect = pygame.rect.Rect(
            pygame.Vector2(self._HEALTHBAR_POSITION.x, self._HEALTHBAR_POSITION.y),
            pygame.Vector2(self._HEALTHBAR_SIZE.x, self._HEALTHBAR_SIZE.y))
        pygame.draw.rect(pygame.display.get_surface(), (255, 0, 0), rect)

        # green foreground
        health_scale = PlayerManager.PlayerManager.get_instance().get_health_scale()
        rect.width = self._HEALTHBAR_SIZE.x * health_scale
        pygame.draw.rect(pygame.display.get_surface(), (0, 255, 0), rect)

        # render shield
        shield_scale = PlayerManager.PlayerManager.get_instance().get_shield_scale()
        rect.width = self._HEALTHBAR_SIZE.x * shield_scale
        pygame.draw.rect(pygame.display.get_surface(), (0, 0, 255), rect)

    def render_crosshair(self) -> None:
        rect = self._crosshair.get_rect()
        rect.x = self._input.get_mouse_x() - (rect.width / 2.0)
        rect.y = self._input.get_mouse_y() - (rect.height / 2.0)
        pygame.display.get_surface().blit(self._crosshair, rect)
