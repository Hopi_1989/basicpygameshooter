import pygame
import MyUtils
from Constants import GAME_FONT


class Scoreboard:
    _instance: 'Scoreboard' = None

    @staticmethod
    def get_instance() -> 'Scoreboard':
        if Scoreboard._instance is None:
            Scoreboard._instance = Scoreboard()

        return Scoreboard._instance

    def __init__(self):
        self._score = 0
        self._font_color = (255, 255, 255)
        self._position = MyUtils.Point(25, 75)
        self._font = pygame.font.Font(GAME_FONT, 24)

    def reset_score(self) -> None:
        self._score = 0

    def get_score(self) -> int:
        return self._score

    def increment_score(self, amount: int = 1) -> None:
        self._score += amount

    def set_position(self, position: MyUtils.Point) -> None:
        self._position = position

    def render(self) -> None:
        if self._font is None:
            return

        text = self._font.render(f"Score: {self._score}", True, self._font_color, None)
        rect = text.get_rect()
        rect.x = self._position.x
        rect.y = self._position.y
        pygame.display.get_surface().blit(text, rect)
