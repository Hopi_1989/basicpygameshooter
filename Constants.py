import os


SCREEN_WIDTH: int = 1024
SCREEN_HEIGHT: int = 768
FRAME_RATE: int = 60
GAME_FONT: str = os.path.join("Assets", "Fonts", "Orbitron-Medium.ttf")
