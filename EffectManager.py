import MyUtils
from Effect import Effect, EffectType


class EffectManager:
    _instance = None

    @staticmethod
    def get_instance() -> 'EffectManager':
        if EffectManager._instance is None:
            EffectManager._instance = EffectManager()
        return EffectManager._instance

    def __init__(self):
        Effect.load_effects()
        self._effects: list[Effect] = []

    def create_effect(self, position: MyUtils.Point, effect_type: EffectType, scale: float) -> None:
        self._effects.append(Effect(position, effect_type, scale))

    def create_enemy_explosion(self, position: MyUtils.Point, scale: float = 1.0) -> None:
        self._effects.append(Effect(position, EffectType.EXPLOSION_ENEMY, scale))

    def create_player_explosion(self, position: MyUtils.Point, scale: float = 1.0) -> None:
        self._effects.append(Effect(position, EffectType.EXPLOSION_PLAYER, scale))

    def update(self) -> None:
        self._effects = [effect for effect in self._effects if effect.is_active()]

        for effect in self._effects:
            effect.update()

    def render(self) -> None:
        for effect in self._effects:
            if effect.is_active():
                effect.render()
